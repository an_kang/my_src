# 变更履历



## V0.0.0

草稿版，已完成装机

## V0.1.0

确定防尘网的链接方式，优化主板固定销，配色改为灰白相间

## V0.2.0

封箱基本完成。

![Snipaste_2022-05-17_15-25-40](.\pic1.png)

## V0.3.0

两侧封箱亚克力板的固定和开孔确定。

## V0.4.0

全新的防尘思路。期望实现开放与封闭的结合，结什么合！不就是半封闭么。

![1653494742710](.\1653494742710.png)

